package ExercicioPratico;

public class Administrador extends Empregado {
	public double ajudaDeCusto;
	
	public double getAjudaDeCusto() {
		return ajudaDeCusto;
	}

	public void setAjudaDeCusto(double ajudaDeCusto) {
		this.ajudaDeCusto = ajudaDeCusto;
	}

	public double calcularSalario() {
		return getSalario() + ajudaDeCusto;

	}

}
