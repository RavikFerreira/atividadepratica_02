package ExercicioPratico;

public class Fornecedor extends Pessoa{
	public double valorCredito;
	public double valorDivida;
	
	public double getValorCredito() {
		return valorCredito;
	}
	public void setValorCredito(double valorCredito) {
		this.valorCredito = valorCredito;
	}
		
	public double getValorDivida() {
		return valorDivida;
	}
	public void setValorDivida(double valorDivida) {
		this.valorDivida = valorDivida;
	}
		
	
	public double obterSaldo() {
		  return valorCredito - valorDivida;
		 
	}
	
}
