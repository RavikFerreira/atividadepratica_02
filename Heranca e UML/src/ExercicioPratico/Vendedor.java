package ExercicioPratico;

public class Vendedor extends Empregado {
	public double valorVendas;
	public double comissao;
	
	public double getValorVendas() {
		return valorVendas;
	}
	public void setValorVendas(double valorVendas) {
		this.valorVendas = valorVendas;
	}
	public double getComissao() {
		return comissao;
	}
	public void setComissao(double comissao) {
		this.comissao = comissao;
	}
	public double calcularSalario() {
		return getSalario() + (comissao * valorVendas)/100;
	}
}
