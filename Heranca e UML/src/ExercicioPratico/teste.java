package ExercicioPratico;

import java.util.Scanner;

public class teste {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		
		Pessoa pessoa = new Pessoa();
		System.out.println("---------------------------------------------");
		System.out.println("Digite seu Nome: ");
		pessoa.nome = sc.nextLine();
		System.out.println("Digite seu CPF: ");
		pessoa.cpf = sc.nextLine();
		System.out.println("Digite seu Endere�o: ");
		pessoa.endereco = sc.nextLine();
		System.out.println("Digite sua Data de Nascimento: ");
		pessoa.nascimento = sc.nextLine();
		System.out.println("Digite seu N�mero de telefone: ");
		pessoa.telefone = sc.nextLine();
		System.out.println();
		
		Fornecedor fornecedor = new Fornecedor();
		System.out.println("---------------------------------------------");
		System.out.println("Nome do Fornecedor: ");
		fornecedor.nome = sc.nextLine();
		System.out.println("Digite o CPF: ");
		fornecedor.cpf = sc.nextLine();
		System.out.println("Endere�o: ");
		fornecedor.endereco = sc.nextLine();
		System.out.println("Telefone: ");
		fornecedor.telefone = sc.nextLine();
		System.out.println("Digite o valor do Cr�dito: ");
		fornecedor.valorCredito = sc.nextDouble();
		System.out.println("Digite o valor da d�vida: ");
		fornecedor.valorDivida = sc.nextDouble();
		System.out.printf("O Saldo total �: %.2f\n",fornecedor.obterSaldo());
		System.out.println();
		
		Administrador admin = new Administrador();
		System.out.println("---------------------------------------------");
		System.out.println("Nome Administrador: ");
		admin.nome = sc.nextLine();
		System.out.println("Digite o CPF: ");
		admin.cpf = sc.nextLine();
		System.out.println("Telefone: ");
		admin.telefone = sc.nextLine();
		System.out.println("Endere�o: ");
		admin.endereco = sc.nextLine();
		System.out.println("Salario: ");
		admin.salario = sc.nextDouble();
		System.out.println("Ajuda de Custo: ");
		admin.ajudaDeCusto = sc.nextDouble();
		System.out.println("Saldo Total: "+admin.calcularSalario());
		System.out.println();
		
		Vendedor vendedor = new Vendedor();
		System.out.println("----------------------------------------------");
		System.out.println("Nome Vendedor: ");
		vendedor.nome = sc.nextLine();
		System.out.println("Digite o CPF: ");
		vendedor.cpf = sc.nextLine();
		System.out.println("Endere�o: ");
		vendedor.endereco = sc.nextLine();
		System.out.println("Telefone; ");
		vendedor.telefone = sc.nextLine();
		System.out.println("Sal�rio: ");
		vendedor.salario = sc.nextDouble();
		System.out.println("Valor em Vendas: ");
		vendedor.valorVendas = sc.nextDouble();
		System.out.println("Valor em Porcentagem: ");
		vendedor.comissao = sc.nextDouble();
		System.out.println("Saldo Total: "+vendedor.calcularSalario());
		System.out.println("-----------------------------------------------");
	
		sc.close();
	}

}
